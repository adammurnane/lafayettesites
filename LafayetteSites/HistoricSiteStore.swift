//
//  HistoricSiteStore.swift
//  LafayetteSites
//
//  Created by Adam Murnane on 3/29/17.
//  Copyright © 2017 Waitr. All rights reserved.
//

import Foundation
import CoreLocation

class HistoricSiteStore {
    static let sharedStore = HistoricSiteStore()
    
    private(set) var sites: [HistoricSite] = []
    
    @discardableResult func addHistoricSite(json: [String : Any]) -> HistoricSite {
        // Parse the JSON object into a model object
        //"attributes": {
        //    "OBJECTID": 15920,
        //    "ID": 1102,
        //    "NAME": "LAFAYETTE WHOLESALE GROCERY WAREHOUSE",
        //    "ADDRESS": "333 MONROE ST",
        //    "ARCH_TYPE": "COMMERCIAL VERNACULAR",
        //    "CONST_DATE": "CIRCA 1926",
        //    "PHOTOS": null,
        //    "SDE_PHOTOS": null,
        //    "NUM_ON_REGISTER": 102,
        //    "DESIGNATION_DATE": 1424304000000 (millis since epoch)
        //},
        //"geometry": {
        //    "x": -92.01710131083995,
        //    "y": 30.229238935287576
        //}
        // For JSON info please see http://codefest-cgifederal.opendata.arcgis.com/datasets/bc42907ec341416bb193a73691df1029_0/geoservice?geometry=-92.561%2C30.108%2C-91.572%2C30.316
        
        let site = HistoricSite()
        
        let attributes = json["attributes"] as! [String : Any]
        site.objectId = attributes["OBJECTID"] as? UInt
        site.id = attributes["ID"] as? UInt
        site.name = attributes["NAME"] as? String
        site.address = attributes["ADDRESS"] as? String
        site.architectureType = attributes["ARCH_TYPE"] as? String
        site.constructionDateString = attributes["CONST_DATE"] as? String
        site.photoName = attributes["PHOTOS"] as? String
        if let urlString = attributes["SDE_PHOTOS"] as? String {
            site.photoUrl = URL(string: urlString)
        }
        site.numberOnRegister = attributes["NUM_ON_REGISTER"] as? UInt
        if let dateInMillis = attributes["DESIGNATION_DATE"] as? UInt64 {
            let dateInSec = dateInMillis / 1000
            site.designationDate = NSDate(timeIntervalSince1970: TimeInterval(dateInSec))
        }
        
        let geometry = json["geometry"] as! [String : Double]
        site.coord = CLLocationCoordinate2DMake(geometry["y"]!, geometry["x"]!)
        
        sites.append(site)
        
        return site
    }
}


//
//  HistoricSite.swift
//  LafayetteSites
//
//  Created by Adam Murnane on 3/29/17.
//  Copyright © 2017 Waitr. All rights reserved.
//

import Foundation
import CoreLocation

class HistoricSite {
    var objectId: UInt?
    var id: UInt?
    var name: String?
    var address: String?
    var architectureType: String?
    var constructionDateString: String?
    var photoName: String?
    var photoUrl: URL?
    var numberOnRegister: UInt?
    var designationDate: NSDate?
    var coord: CLLocationCoordinate2D?
}

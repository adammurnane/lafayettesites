//
//  ApiManager.swift
//  LafayetteSites
//
//  Created by Adam Murnane on 3/29/17.
//  Copyright © 2017 Waitr. All rights reserved.
//

import Foundation
import Alamofire

class ApiManager {
    static let sharedManager = ApiManager()
    let apiUrl = "https://services.arcgis.com/fOr4AY8t0ujnJsua/ArcGIS/rest/services/LCG_preservation_sites/FeatureServer/0/query?where=1%3D1&outFields=*&outSR=4326&f=json"
    
    func fetchHistoricSites(completion: ((_ success: Bool, _ error: Error?) -> Void)!) {
        Alamofire.request(apiUrl).responseJSON { response in            
            if let json = response.result.value as? [String: Any],
                    let features = json["features"] as? [Dictionary<String, Any>] {
                for siteDict in features {
                    HistoricSiteStore.sharedStore.addHistoricSite(json: siteDict)
                }
                completion(true, nil)
            } else {
                // An error occured
                completion(false, response.result.error)
            }
        }
    }
}

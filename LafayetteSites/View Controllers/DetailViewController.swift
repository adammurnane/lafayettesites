//
//  DetailViewController.swift
//  LafayetteSites
//
//  Created by Adam Murnane on 3/30/17.
//  Copyright © 2017 Waitr. All rights reserved.
//

import UIKit
import SDWebImage

class DetailViewController: UIViewController {
    @IBOutlet var photoImageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var constDateLabel: UILabel!
    @IBOutlet var addressLabel: UILabel!
    
    var historicSite: HistoricSite?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup subviews
        if let url = self.historicSite?.photoUrl {
            photoImageView.sd_setImage(with: url)
        } else {
            photoImageView.image = UIImage(named: "image-unavailable")
        }
        if let name = self.historicSite?.name {
            nameLabel.text = name
        } else {
            nameLabel.text = "<NAME NOT AVAILABLE>"
        }
        if let constDate = self.historicSite?.constructionDateString {
            constDateLabel.text = constDate
        } else {
            constDateLabel.text = "<CONSTRUCTION DATE NOT AVAILABLE>"
        }
        if let address = self.historicSite?.address {
            addressLabel.text = address
        } else {
            addressLabel.text = "<ADDRESS NOT AVAILABLE>"
        }
    }
}

//
//  MapViewController.swift
//  LafayetteSites
//
//  Created by Adam Murnane on 3/29/17.
//  Copyright © 2017 Waitr. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import SVProgressHUD

class MapViewController: UIViewController, MKMapViewDelegate {
    @IBOutlet var mapView: MKMapView!
    @IBOutlet var viewTypeSegmentedControl: UISegmentedControl!
    
    let lafayetteCenterCoord = CLLocationCoordinate2DMake(30.2167, -92.033)
    
    var selectedSite: HistoricSite?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Fix the segmented control
        viewTypeSegmentedControl.layer.masksToBounds = true
        viewTypeSegmentedControl.layer.cornerRadius = 4.0
        
        // Center the map on Lafayette.
        let span = MKCoordinateSpan(latitudeDelta: 0.2, longitudeDelta: 0.2)
        let region = MKCoordinateRegion(center: lafayetteCenterCoord, span: span)
        mapView.setRegion(region, animated: false)
        mapView.mapType = MKMapType.standard
        
        // Make API call
        SVProgressHUD.show()
        ApiManager.sharedManager.fetchHistoricSites { (success, error) in
            SVProgressHUD.dismiss()
            if success {
                self.setAnnotations()
            } else {
                let alertController = UIAlertController(title: "Error",
                                                        message: "Unable to reach the server.",
                                                        preferredStyle: UIAlertControllerStyle.alert)
                alertController.show(self, sender: self)
            }
        }
    }

    func setAnnotations() {
        // Create a map annotation for each historic site
        for site in HistoricSiteStore.sharedStore.sites {
            if let coord = site.coord, let title = site.name {
                let annotation = HistoricSiteAnnotation(site)
                annotation.coordinate = coord
                annotation.title = title
                mapView.addAnnotation(annotation)
            }
        }
    }
    
    // MARK: Control Events
    @IBAction func viewTypeSegmentedControlValueChanged(_ sender: Any) {
        if viewTypeSegmentedControl.selectedSegmentIndex == 0 {
            mapView.mapType = MKMapType.standard
        } else {
            mapView.mapType = MKMapType.satellite
        }
    }
    
    func infoButtonTapped(_ sender: Any) {
        if selectedSite != nil {
            // Show site details
            print("\(String(describing: selectedSite!.name)) info button tapped!")
            let detailVC = self.storyboard?.instantiateViewController(withIdentifier: "detailVC")
                    as! DetailViewController
            detailVC.historicSite = selectedSite
            self.navigationController?.pushViewController(detailVC, animated: true)
        }
    }
    
    // MARK: Map View Delegate
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "annotationView")
        if annotationView == nil {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "annotationView")
        }
        
        annotationView?.canShowCallout = true
        
        // Add an info button
        let rightInfoButton = UIButton(type: UIButtonType.detailDisclosure)
        rightInfoButton.addTarget(self, action: #selector(MapViewController.infoButtonTapped(_:)),
                                  for: UIControlEvents.touchUpInside)
        annotationView?.rightCalloutAccessoryView = rightInfoButton
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        let annotation = view.annotation as! HistoricSiteAnnotation
        selectedSite = annotation.historicSite
    }
}


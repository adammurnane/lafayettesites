//
//  HistoricSiteAnnotation.swift
//  LafayetteSites
//
//  Created by Adam Murnane on 3/29/17.
//  Copyright © 2017 Waitr. All rights reserved.
//

import UIKit
import MapKit

class HistoricSiteAnnotation: MKPointAnnotation {
    let historicSite: HistoricSite
    
    required init(_ historicSite: HistoricSite) {
        self.historicSite = historicSite
        super.init()
    }
}
